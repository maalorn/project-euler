package main

import (
    // "fmt"
	"reflect"
	"testing"
)

type IntIntCase struct {
    in int64
    want int64
}

type IntBoolCase struct {
	in   int64
	want bool
}

type IntIntsCase struct {
	in   int64
	want []int64
}

func TestFact(t *testing.T) {
    cases := []IntIntCase{
        {2, 2},
        {3, 6},
        {4, 24},
        {1, 1},
    }

	for _, c := range cases {
		got := Fact(c.in)
		if got != c.want {
			t.Errorf("Fact(%d) == %v, want %v\n", c.in, got, c.want)
		}
	}
}

func TestIsPalindrome(t *testing.T) {
    cases := []IntBoolCase{
        {1000, false},
        {1001, true},
        {1005001, true},
        {234923408, false},
    }

	for _, c := range cases {
		got := IsPalindrome(c.in)
		if got != c.want {
			t.Errorf("IsPalindrome(%d) == %v, want %v\n", c.in, got, c.want)
		}
	}
}

func TestRabinMiller(t *testing.T) {
	cases := []IntBoolCase{
		{1, false},
		{2, true},
		{3, true},
		{4, false},
		{5, true},
		{6, false},
		{7, true},
		{8, false},
	}

	for _, c := range cases {
		got := IsPrimeMR(c.in, 6)
		if got != c.want {
			t.Errorf("IsPrimeMR(%d) == %v, want %v\n", c.in, got, c.want)
		}
	}
}

func TestBitsOfN(t *testing.T) {
	cases := []IntIntsCase{
		{1, []int64{1}},
		{2, []int64{0, 1}},
		{3, []int64{1, 1}},
		{25, []int64{1, 0, 0, 1, 1}},
	}

	for _, c := range cases {
		got := BitsN(c.in)
		if !reflect.DeepEqual(got, c.want) {
			t.Errorf("BitsN(%d) == %v, want %v\n", c.in, got, c.want)
		}
	}
}

func TestReverseBitsOfN(t *testing.T) {
	cases := []IntIntsCase{
		{1, []int64{1}},
		{2, []int64{1, 0}},
		{3, []int64{1, 1}},
		{25, []int64{1, 1, 0, 0, 1}},
	}

	for _, c := range cases {
		got := BitsN(c.in)
		i, j := len(got)-1, 0
		for i >= j {
			got[i], got[j] = got[j], got[i]
			i--
			j++
		}

		if !reflect.DeepEqual(got, c.want) {
			t.Errorf("RevBitsN(%d) == %v, want %v\n", c.in, got, c.want)
		}
	}
}
