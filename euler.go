package main

import (
    "math"
    "flag"
    "os"
    "fmt"
    "sync/atomic"
    "time"
    "sync"
    "runtime"
    "math/rand"
)

var problemFlag int
var funcs []func()

func init() {
    const (
        defaultProblem = 0
    )
    flag.IntVar(&problemFlag, "p", defaultProblem, "problem number for which to run the solution")
}

func p1() {
    var total int64 = 0
    var i int64 = 0
    var wg sync.WaitGroup

    for i = 0; i < 1000; i++ {
        wg.Add(1)
        go func(i int64) {
            defer wg.Done()
            if mult3or5(i) {
                atomic.AddInt64(&total, i)
            }

            runtime.Gosched()
        }(i)
    }
    wg.Wait()

    finalTotal := atomic.LoadInt64(&total)
    fmt.Println("total: ", finalTotal)

}

func p2() {
    var f1    int64 = 1
    var f2    int64 = 1
    var curr  int64 = f1 + f2
    var total int64 = 0

    for {
        if curr > 4000000 {
            fmt.Println("Total: ", total)
            return 
        }
        if (curr % 2 == 0) {
            total += curr
        }
        f1 = f2
        f2 = curr
        curr = f1 + f2
    }
}

func p3() {
    var theVal int64 = 600851475143
    var start int64 = int64(math.Sqrt(float64(theVal))) - 1
    var highest int64 = 0
    var mutex = &sync.Mutex{}
    var wg sync.WaitGroup

    for j := start; j > 0; j -= 2 {
        wg.Add(1)
        go func(i int64) {
            defer wg.Done()
            if (theVal % i == 0) && IsPrimeMR(i, 6) {
                currHighest := atomic.LoadInt64(&highest)
                mutex.Lock()
                if i > currHighest {
                    atomic.StoreInt64(&highest, i)
                }
                mutex.Unlock()
            }
        }(j)
    }
    wg.Wait()

    theHighest := atomic.LoadInt64(&highest)
    fmt.Println("Factor: ", theHighest)
}

func p4() {
    var wg sync.WaitGroup
    var mutex = &sync.Mutex{}
    var highest int64 = 0

    for i := 100; i < 1000; i++ {
        for j := 100; j < 1000; j++ {
            wg.Add(1)
            go func(i, j int) {
                defer wg.Done()
                prod := int64(i) * int64(j)
                if IsPalindrome(prod) {
                    currHighest := atomic.LoadInt64(&highest)
                    mutex.Lock()
                    if prod > currHighest {
                        atomic.StoreInt64(&highest, prod)
                    }
                    mutex.Unlock()
                }
            }(i, j)
        }
    }
    wg.Wait()

    theHighest := atomic.LoadInt64(&highest)
    fmt.Println("Palindrome: ", theHighest)
}

func p5() {
    var wg sync.WaitGroup
    var mutex = &sync.Mutex{}
    var highest int64 = Fact(20)
    var smallest int64 = highest

    var i int64
    for i = 20; i < highest; i++ {
        wg.Add(1)
        go func(i int64) {
            defer wg.Done()
            if IsPrimeMR(i, 6) {
                return
            }
            if divFact20(i) {
                currSmallest := atomic.LoadInt64(&smallest)
                mutex.Lock()
                if i < currSmallest {
                    atomic.StoreInt64(&smallest, i)
                    fmt.Println("Found: ", currSmallest)
                }
                mutex.Unlock()
            }
        }(i)
    }
    wg.Wait()

    theSmallest := atomic.LoadInt64(&smallest)
    fmt.Println("Answer: ", theSmallest)
}

func runAll() {
    for i,_ := range funcs {
        runOne(i+1)
    }
}

func runOne(i int) {
    fmt.Printf("Running solution for problem #%d...\n", i)
    if i > len(funcs) {
        fmt.Println("Problem number not implemented!")
        os.Exit(1)
    }

    func() {
        defer un(trace(fmt.Sprintf("Problem %d", i)))
        funcs[i-1]()
    }()
    fmt.Println()
}

func main() {
    funcs = make([]func(), 0)
    funcs = append(funcs, p1)
    funcs = append(funcs, p2)
    funcs = append(funcs, p3)
    funcs = append(funcs, p4)
    funcs = append(funcs, p5)

    flag.Parse()
    if problemFlag == 0 {
        runAll()
    } else {
        runOne(problemFlag)
    }
}

func IsPalindrome(n int64) bool {
    s := fmt.Sprintf("%d", n)
    for i := 0; i < len(s) / 2; i++ {
        if s[i] != s[len(s)-1-i] {
            return false
        }
    }
    return true
}

func mult3or5(n int64) bool {
    if (n % 3 == 0) || (n % 5 == 0) {
        return true
    } else {
        return false
    }
}

func BitsN(n int64) []int64 {
    var bits []int64
    bits = make([]int64, 0)
    for {
        if n == 0 {
            return bits
        }

        bits = append(bits, n % 2)
        n >>= 1
    }
}

func MRCompWit(a int64, n int64) bool {
    var rem int64 = 1
    bits := BitsN(n-1)
    for i := len(bits)-1; i >= 0; i-- {
        b := bits[i]
        x := rem
        rem = (rem * rem) % n


        if rem == 1 && x != 1 && x != (n-1) {
            return true
        }

        if b == 1 {
            rem = (rem * a) % n
        }
    }

    if rem != 1 {
        return true
    }
    return false
}

func IsPrimeMR(n int64, trials int) bool {
    if n < 2 {
        return false
    }

    for i := 0; i < trials; i++ {
        if MRCompWit(rand.Int63n(n-1) + 1, n) {
            return false
        }
    }
    return true
}

func trace(s string) (string, time.Time) {
	fmt.Println("START:", s)
	return s, time.Now()
}

func un(s string, startTime time.Time) {
	endTime := time.Now()
	fmt.Println("  END:", s, "- Elapsed time:", endTime.Sub(startTime))
}

func Fact(n int64) int64 {
    var total int64 = 1
    var i int64
    for i = 2; i < n+1; i++ {
        total *= i
    }
    return total
}

func divFact20(n int64) bool {
    var i int64
    for i = 11; i < 21; i++ {
        if n % i != 0 {
            return false
        }
    }
    return true
}
